//This works
int redPin = 4;
int greenPin = 3;
int bluePin = 2;

void setup() {
  Serial.begin(9600);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}
int n = 0;
int count;
int red,green,blue;

void loop() {
  red = 255; green = 0; blue = 0;
  setColor(red,green,blue);
  delay(1000);
  red = 0; green = 255; blue = 0;
  setColor(red,green,blue);
  delay(1000);
  red = 0; green = 0; blue = 255;
  setColor(red,green,blue);
  delay(1000);
  red = 255; green = 0; blue = 255;
  setColor(red,green,blue);
  delay(1000);
  red = 128; green = 0; blue = 128;
  setColor(red,green,blue);
  delay(1000);
  red = 255; green = 255; blue = 0;
  setColor(red,green,blue);
  delay(1000);
  
  
}

void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}
